package com.devcamp.voucher.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vouchers")
public class CVoucher {    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="phan_tram_giam_gia")
    private String phanTramGiamGia;

    @Column(name="ghi_chu")
    private String ghiChu;

    @Column(name="ma_voucher")
    private String maVoucher;

    @Column(name="ngay_tao")
    private Long ngayTao;
    
    @Column(name="ngay_cap_nhat")
    private Long ngayCapNhat;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getPhanTramGiamGia() {
        return phanTramGiamGia;
    }
    public void setPhanTramGiamGia(String phanTramGiamGia) {
        this.phanTramGiamGia = phanTramGiamGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public String getMaVoucher() {
        return maVoucher;
    }
    public void setMaVoucher(String maVoucher) {
        this.maVoucher = maVoucher;
    }
    public Long getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(Long ngayTao) {
        this.ngayTao = ngayTao;
    }
    public Long getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(Long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
}
