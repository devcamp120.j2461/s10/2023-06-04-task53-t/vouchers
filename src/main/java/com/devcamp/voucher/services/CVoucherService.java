package com.devcamp.voucher.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.voucher.entities.CVoucher;
import com.devcamp.voucher.repositories.IVoucherRepository;

@Service
public class CVoucherService {
    @Autowired
    private IVoucherRepository voucherRepository;

    public ArrayList<CVoucher> getAllVoucher() {
        ArrayList<CVoucher> cVouchers = new ArrayList<>();
        this.voucherRepository.findAll().forEach(cVouchers::add);

        return cVouchers;
    }
}
