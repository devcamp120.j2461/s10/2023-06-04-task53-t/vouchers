package com.devcamp.voucher.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucher.entities.CVoucher;
import com.devcamp.voucher.services.CVoucherService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CVoucherController {
    @Autowired
    private CVoucherService cVoucherService;

    @GetMapping("/vouchers")
    public ResponseEntity<ArrayList<CVoucher>> getVouchers() {
        CVoucher voucher = new CVoucher();
        voucher.setGhiChu("aaa");
        
        return new ResponseEntity<>(this.cVoucherService.getAllVoucher(), HttpStatus.OK);
    }
}
